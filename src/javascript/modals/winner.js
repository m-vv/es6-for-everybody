import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  // show winner name and image
  const title = 'Winner Fighter info';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter) {
  const { name, source } = fighter;
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  nameElement.innerText = name;
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  fighterContainer.append(nameElement, imgElement );
  fighterDetails.append(fighterContainer);
  
  return fighterDetails;
}