import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';


export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  nameElement.innerText = name;
  const fighterCharacteristicListElement = createElement({ tagName: 'ul' });
  const attackli = createElement({ tagName: 'li' });
  attackli.innerText = `attack: ${attack}`;
  fighterCharacteristicListElement.append(attackli);
  const defenseli = createElement({ tagName: 'li' });
  defenseli.innerText = `defense: ${defense}`;
  fighterCharacteristicListElement.append(defenseli);
  const healthli = createElement({ tagName: 'li' });
  healthli.innerText = `health: ${health}`;
  fighterCharacteristicListElement.append(healthli);
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  fighterDetails.append(nameElement);
  fighterDetails.append(fighterCharacteristicListElement);
  fighterDetails.append(imgElement);

  return fighterDetails;
}
