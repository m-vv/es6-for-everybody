export function fight(firstFighter, secondFighter) {
  
  let turn = true //if true first make hit if false second
  while (true) {
    if (turn) {
      secondFighter.health -= getDamage(firstFighter, secondFighter);
      if (secondFighter.health < 0) {
        return firstFighter;        
      }
      turn = false;
    } else {
      firstFighter.health -= getDamage(secondFighter, firstFighter);
      if (firstFighter.health < 0) {
        return secondFighter;        
      }
      turn = true;
        }
  }
}

export function getDamage(attacker, enemy) {
  
  return getHitPower(attacker) - getBlockPower(enemy);

}

export function getHitPower(fighter) {
  
  return fighter.attack * randomNumber(1,2);
}

export function getBlockPower(fighter) {
  
  return fighter.defense * randomNumber(1,2);
}

function randomNumber(min, max) {

  return Math.random() * (max - min) + min;
}